#include <GL/glut.h>
#include <stdio.h>

int width = 800;
int height = 800;
int nRegions = 0;
int *vtx;
float **pos[2];

void readFile(const char *filename) {
  FILE *file;
  file = fopen(filename, "r");

  char str[1000];
  if (file) {
    fscanf(file, "%d", &nRegions);
    vtx = new int[nRegions];
    for (int i = 0; i < 2; i += 1) {
      pos[i] = new float*[nRegions];
    }

    for (int i = 0; i < nRegions; i += 1) {
      fscanf(file, "%d", &vtx[i]);
      for (int j = 0; j < 2; j += 1) {
        pos[j][i] = new float[vtx[i]];
      }

      for (int j = 0; j < vtx[i]; j += 1) {
        fscanf(file, "%f", &pos[0][i][j]);
        fscanf(file, "%f", &pos[1][i][j]);
      }
    }

    fclose(file);
  } 
}

void initGL()
{
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  gluOrtho2D(0.0, 800.0, 0.0, 800.0);
  glClearColor(1.0, 1.0, 1.0, 1.0);
  glShadeModel(GL_SMOOTH);
  glViewport(0, 0, width, height);
}

void display()
{ 
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  glMatrixMode(GL_PROJECTION);
  glColor3f(0.0, 0.0, 0.0);

  for (int i = 0; i < nRegions; i += 1) {
    glBegin(GL_LINE_STRIP);
    for (int j = 0; j < vtx[i]; j += 1) {
        glVertex2f(pos[0][i][j], pos[1][i][j]);
    }
    glEnd();
  }

  glutSwapBuffers();
}

void reshape(int w, int h) {
  glutReshapeWindow(w, w);
  width = w;
  height = w;
  initGL();   
}


int main(int argc, char ** argv)
{
  readFile("dinosaur.dat");

  glutInit(&argc, argv);
  glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE);
  glutInitWindowSize(width, height);
  glutInitWindowPosition(100, 100);
  glutCreateWindow("14020072 Dinosaur");

  initGL();

  glutDisplayFunc(display);
  glutReshapeFunc(reshape);

  glutMainLoop();
}
#include <GL/glut.h>
#include <math.h>

float oldx = -0.7, oldy = 0.5;

int width = 500;
int height = 500;

void initGL() {
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  glClearColor(1.0, 1.0, 1.0, 1.0);
  glShadeModel(GL_SMOOTH);
  glViewport(0, 0, width, height);
}

void drawkoch(float dir, float len, int iter) {
  GLdouble dirRad = 0.0174533 * dir;  
  GLfloat newX = oldx + len * cos(dirRad);
  GLfloat newY = oldy + len * sin(dirRad);
  if (iter == 0) {
    glVertex2f(oldx, oldy);
    glVertex2f(newX, newY);
    oldx = newX;
    oldy = newY;
  }
  else {
    iter--;
    drawkoch(dir, len, iter);
    dir += 60.0;
    drawkoch(dir, len, iter);
    dir -= 120.0;
    drawkoch(dir, len, iter);
    dir += 60.0;
    drawkoch(dir, len, iter);
  }
}

void display() {
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  glMatrixMode(GL_PROJECTION);

  glBegin(GL_LINES);
  glColor3f(1.0, 0.0, 0.0); 

  drawkoch(0.0, 0.05, 3);
  drawkoch(-120.0, 0.05, 3);
  drawkoch(120.0, 0.05, 3);
  glEnd();

  glutSwapBuffers();
}

void reshape(int w, int h) {
  glutReshapeWindow(w, w);
  width = w;
  height = h;
  initGL();
}


int main(int argc, char** argv)
{
  glutInit(&argc, argv); 
  glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB);      
  glutInitWindowSize(width, height);      
  glutInitWindowPosition(100, 100); 
  glutCreateWindow("14020072 Snowflake");     

  initGL();

  glutDisplayFunc(display);  
  glutReshapeFunc(reshape);
  glutMainLoop();
}
#include <GL/glut.h>
#include<vector>
using namespace std;

struct p2f {
	double x;
	double y;
public:
	void move(double dx, double dy) {
		x += dx;
		y += dy;
	}
};

vector<p2f> shape_one = { { 0.2,-0.9 },{ 0,0 },{ 0, 0.8 },{ 0,0.7 },
{ -0.5,0.2 },{ 0.1, -0.1 },{ -0.3,-0.3 },{ -0.5, -0.9 },{ -0.1, -0.3 } ,{-0.7,-0.9}, {0,-0.9} };

vector<p2f> shape_five = { { -0.8,-0.9 },{ 0,0 },{ 0, 0.8 },{ 0,0.7 },
{ 0.6,0.2 },{ -0.2, 0.15 },{ -0.05,-0.3 },{ 0.1, -0.9 },{ -0.4, -0.2 }, {-0.1,-0.9}, {-0.9,-0.9} };

vector<p2f> shape_three = { {0,0}, {0,0},{ 0,0 },{ 0,0 },{ 0,0 },{ 0,0 },{ 0,0 },{ 0,0 },{ 0,0 },{ 0,0 },{ 0,0 } };
vector<p2f> shape_two = { { 0,0 },{ 0,0 },{ 0,0 },{ 0,0 },{ 0,0 },{ 0,0 },{ 0,0 },{ 0,0 },{ 0,0 },{ 0,0 },{ 0,0 } };
vector<p2f> shape_four = { { 0,0 },{ 0,0 },{ 0,0 },{ 0,0 },{ 0,0 },{ 0,0 },{ 0,0 },{ 0,0 },{ 0,0 },{ 0,0 },{ 0,0 } };

int n = 1;

void move(vector<p2f> &shape, double dx, double dy) {
	for (int i = 0; i < shape.size(); i++) {
		shape[i].move(dx, dy);
	}
}

void init(void) {
	glClearColor(0.0, 0.0, 0.0, 1.0);
	for (int i = 0; i < 11; i++) {
		shape_three[i].x = 0.5*(shape_one[i].x + shape_five[i].x);
		shape_three[i].y = 0.5*(shape_one[i].y + shape_five[i].y);
	}
	for (int i = 0; i < 11; i++) {
		shape_two[i].x = 0.5*(shape_one[i].x + shape_three[i].x);
		shape_two[i].y = 0.5*(shape_one[i].y + shape_three[i].y);
	}
	for (int i = 0; i < 11; i++) {
		shape_four[i].x = 0.5*(shape_three[i].x + shape_five[i].x);
		shape_four[i].y = 0.5*(shape_three[i].y + shape_five[i].y);
	}
	move(shape_five, -0.2, 0);
	move(shape_three, -0.1, 0);
	move(shape_four, -0.15, 0);
	move(shape_two, -0.05, 0);

}

void draw2Dperson(vector<p2f> anchors) {
	// anchors is len-9 vector
	int lines[9][2] = { {0,8}, {8,1}, {1,6}, {6,7}, {1,2},{3,4},{3,5}, {7,9}, {0,10 } };
	glPointSize(50);
	glColor3f(1, 1, 1);
	glBegin(GL_POINTS);
	glVertex2f(anchors[2].x, anchors[2].y);
	glEnd();
	glLineWidth(4);
	glColor3f(1, 0, 0);
	glBegin(GL_LINES);
	for (int i = 0; i < 9; i++) {
		if (i == 6) glColor3f(0, 1, 0);
		if (i == 3 || i == 7 || i == 2) glColor3f(0, 0, 1);
		glVertex2f(anchors[lines[i][0]].x, anchors[lines[i][0]].y);
		glVertex2f(anchors[lines[i][1]].x, anchors[lines[i][1]].y);
		glLineWidth(4);
		glColor3f(1, 0, 0);
	}
	glEnd();
}

void display() {

	glClear(GL_COLOR_BUFFER_BIT);
	if (n == 1) {
		draw2Dperson(shape_one);
		move(shape_one, -0.2, 0);
	}
	if (n == 2) {
		draw2Dperson(shape_two);
		move(shape_two, -0.2, 0);
	}
	if (n == 3) {
		draw2Dperson(shape_three);
		move(shape_three, -0.2, 0);
	}
	if (n == 4) {
		draw2Dperson(shape_four);
		move(shape_four, -0.2, 0);
	}
	if (n == 5) {
		draw2Dperson(shape_five);
		move(shape_five, -0.2, 0);
	}
	if (shape_one[0].x < -1.5) {
		move(shape_one, 4, 0);
		move(shape_two, 4, 0);
		move(shape_three, 4, 0);
		move(shape_four, 4, 0);
		move(shape_five, 4, 0);
	}
	n = (n) % 5 + 1;
	glutSwapBuffers();
	glutPostRedisplay();

}

void reshape(GLsizei width, GLsizei height) { 
	if (height == 0) height = 1;            
	GLfloat aspect = (GLfloat)width / (GLfloat)height;
	glViewport(0, 0, width, height);
	glMatrixMode(GL_PROJECTION);  
	glLoadIdentity();
	if (width >= height) {

		gluOrtho2D(-1.0 * aspect, 1.0 * aspect, -1.0, 1.0);
	}
	else {
		gluOrtho2D(-1.0, 1.0, -1.0 / aspect, 1.0 / aspect);
	}
}


int main(int argc, char** argv) {
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB);
	glutInitWindowSize(1200, 600);
	glutInitWindowPosition(100, 100);
	glutCreateWindow("Moving 2D Person");
	init();
	glutDisplayFunc(display);
	glutReshapeFunc(reshape);
	glutMainLoop();
	return 0;
}
#include <GL/glut.h>
#include <stdio.h>
#include <math.h>

int width = 600;
int height = 600;

float t = 0.0;
float gap = 0.001;

struct Point {
  float x, y;
  Point(float _x, float _y) : x(_x), y(_y) {}
};

void drawCircle(Point center, float r, int loop) {
  glBegin(GL_LINE_LOOP);
    for (int i = 0; i < loop; i += 1) {
      float x = center.x + r * cos(2 * M_PI / loop * i);
      float y = center.y + r * sin(2 * M_PI / loop * i);
      glVertex3f(x, y, 0);
    }
  glEnd();
}

void drawLine(Point start, Point end) {
  glBegin(GL_LINES);
    glVertex3f(start.x, start.y, 0);
    glVertex3f(end.x, end.y, 0);
  glEnd();
}

void initGL() {
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  glClearColor(1.0, 1.0, 1.0, 1.0);
  glShadeModel(GL_SMOOTH);
  glViewport(0, 0, width, height);
}

void display() {
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  glMatrixMode(GL_PROJECTION);

  if (t < 1.0) t += gap;
  for (int i = 0; i < 1000000; i += 1); // Delay;

  glColor3f(0.0, 0.0, 0.0);
  // Head
  drawCircle(Point(-0.5 + t, 0.3), 0.2, 100);
  // Body
  drawLine(Point(-0.5 + t, 0.1), Point(-0.5 + t, -0.3));
  // Left hand
  drawLine(Point(-0.5 + t, 0.0), Point(-0.75 + t, -0.1));
  // Right hand
  drawLine(Point(-0.5 + t, 0.0), Point(-0.25 + t, -0.1));

  float leftLeg = -0.66 + t * 2;
  float rightLeg = -0.34;

  if (leftLeg > rightLeg + 0.32) {
    printf("Done walking!\n");
    exit(1);
  }

  // Left leg
  drawLine(Point(-0.5 + t, -0.3), Point(leftLeg, -0.75));
  // Right leg
  drawLine(Point(-0.5 + t, -0.3), Point(rightLeg, -0.75));

  glutSwapBuffers();
}

void reshape(int w, int h) {
  glutReshapeWindow(w, w);
  width = w;
  height = w;
  initGL();
}

int main(int argv, char * argc[])
{
  glutInit(&argv, argc);
  glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE);
  glutInitWindowSize(width, height);
  glutInitWindowPosition(100, 100);
  glutCreateWindow("14020072 - Human Walking 2D");

  glutDisplayFunc(display);
  glutReshapeFunc(reshape);

  glutMainLoop();
}
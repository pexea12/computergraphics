#include <GL/glut.h>
#include <stdio.h>    

int width = 300;           
int height = 300;
float gap = 0.001;
float t = 0.0;

float begin[9][2] = {
  { -0.8, 0.0 },
  { -0.8, 0.6 },
  { -0.6, 0.6 },
  { -0.6, 0.3 },
  { -0.4, 0.3 },
  { -0.4, 0.6 },
  { -0.2, 0.6 },
  { -0.2, 0.0 },
  { -0.5, -0.3 },
};  

float trans[9][2];

float end[9][2] = {
  { -0.8, 0.0 },
  { -0.6, 0.0 },
  { -0.6, 0.6 },
  { -0.4, 0.6 },
  { -0.4, 0.0 },
  { -0.2, 0.0 },
  { -0.2, -0.2 },
  { -0.5, -0.2 },
  { -0.8, -0.2 },
};     

void display()
{ 
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  glMatrixMode(GL_PROJECTION);

  // Tweening
  for (int i = 0; i < 9; i += 1) {
    trans[i][0] = (1.0 - t) * begin[i][0] + t * end[i][0];
    trans[i][1] = (1.0 - t) * begin[i][1] + t * end[i][1];
  }

  if (t < 1.0)
    t += gap;

  for (int i = 0; i < 1000000; i++); // Delay

  glColor3f(0.0, 0.0, 0.0);
  glBegin(GL_LINE_LOOP); 
    for (int i = 0; i < 9; i += 1) 
      glVertex2f(trans[i][0], trans[i][1]);
  glEnd();

  glColor3f(1.0, 0.0, 0.0);
  glBegin(GL_LINE_LOOP);
    for (int i = 0; i < 9; i += 1)
      glVertex2f(-end[i][0], end[i][1]);
  glEnd();

  glutSwapBuffers();
  glutPostRedisplay();
}

void initGL()
{
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  gluOrtho2D(-1.0f, 1.0f, 1.0f, -1.0f);
  glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
  glShadeModel(GL_SMOOTH);
  glViewport(0, 0, width, height);
}

void reshape(int w, int h)
{
  glutReshapeWindow(w, w);
  width = w;
  height = w;
  initGL();   
}

int main(int argc, char **argv)
{
  glutInit(&argc,argv);   
  glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE);
  glutInitWindowSize(width, height); 
  glutInitWindowPosition(100, 100);
  glutCreateWindow("14020072 Tweening");

  initGL();   

  glutDisplayFunc(display);              
  glutReshapeFunc(reshape);            						
      
  glutMainLoop();				
}





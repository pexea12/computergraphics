#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>
#include <stdlib.h>
#include <stdio.h>

GLfloat angle = 0.0f;
GLfloat speed = 1.0f;

void init(void) 
{
  glClearColor(0.0f, 1.0f, 0.0f, 1.0f); // Yellow
  glShadeModel(GL_FLAT);
}

void display(void)
{
  glClear(GL_COLOR_BUFFER_BIT);
  glPushMatrix();
  glRotatef(angle, 0.0f, 0.0f, 1.0f);
  glColor3f(1.0f, 0.0f, 0.0f);
  glRectf(-10.0f, -10.0f, 10.0f, 10.0f);
  glPopMatrix();
  glutSwapBuffers();
}

void spinDisplay(void)
{
  angle = angle + speed;
  if (angle > 360.0f)
    angle = angle - 360.0f;
  glutPostRedisplay();
}

void reshape(int w, int h)
{
  glViewport(0, 0, (GLsizei) w, (GLsizei) h);
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  glOrtho(-50.0f, 50.0f, -50.0f, 50.0f, -1.0f, 1.0f);
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();
}

void mouse(int button, int state, int x, int y) 
{
  switch (button) {
    case GLUT_LEFT_BUTTON:
      if (state == GLUT_DOWN) 
        glutIdleFunc(spinDisplay);
      if (state == GLUT_UP) 
        speed += 2.0f;
      return;

    case GLUT_RIGHT_BUTTON:
      if (state == GLUT_DOWN)
        glutIdleFunc(NULL);
      return;
   }

   return;
}

int main(int argc, char** argv)
{
  glutInit(&argc, argv);
  glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB);
  glutInitWindowSize(250, 250); 
  glutInitWindowPosition(100, 100);
  glutCreateWindow(argv[0]);

  init();
  printf("Press Left Mouse to rotate, Right Mouse to stop\n");
  printf("Press Left Mouse more times to increase the rotation speed\n");

  glutDisplayFunc(display); 
  glutReshapeFunc(reshape); 
  glutMouseFunc(mouse);
  glutMainLoop();
  return 0;
}
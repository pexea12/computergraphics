#include <GL/glut.h>
#include <math.h>

int width = 600;
int height = 600;

float minX = -10.0;
float maxX = 10.0;
float minY = -3.0;
float maxY = 3.0;

void initGL() {
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  gluOrtho2D(minX, maxX, minY, maxY);
  glClearColor(1.0, 1.0, 1.0, 1.0);
  glShadeModel(GL_SMOOTH);
  glViewport(0, 0, width, height);
}

float f(float x) {
  return sin(x) * 2 + cos(x) * 0.5;
}

void display() {
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  glMatrixMode(GL_PROJECTION);
  glColor3f(0.0, 0.0, 0.0);

  float start = minX;
  float end = maxX;

  glBegin(GL_LINE_STRIP);
    while (start < end) {
      glVertex2f(start, f(start));
      start += .001;
    }
  glEnd();

  glutSwapBuffers();
}

void reshape(int w, int h) {
  glutReshapeWindow(w, w);
  width = w;
  height = h;
  initGL();
}

int main(int argc, char ** argv) 
{
  glutInit(&argc, argv);
  glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB);
  glutInitWindowSize(width, height);
  glutInitWindowPosition(100, 100);
  glutCreateWindow("14020072 Parabol");

  initGL();

  glutDisplayFunc(display);
  glutReshapeFunc(reshape);

  glutMainLoop();
}
#include <stdio.h>
#include <math.h>
#include <GL/glut.h>

int width = 800;
int height = 800;
float K;
int n;

float rose(float theta) {
  return K * cos(n * theta);
}

float calX(float theta) {
  return rose(theta) * cos(theta);
}

float calY(float theta) {
  return rose(theta) * sin(theta);
}

void initGL() {
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  glClearColor(1.0, 1.0, 1.0, 1.0);
  glShadeModel(GL_SMOOTH);
  glViewport(0, 0, width, height);
}

void display() {
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  glMatrixMode(GL_PROJECTION);
  gluOrtho2D(-K, K, -K, K);
  glColor3f(0.0, 0.0, 0.0);

  float start = 0.0;
  float end = M_PI * 2 * n;

  glBegin(GL_POINTS);
  while (start < end) {
    glVertex2f(calX(start), calY(start));
    start += 0.001;
  }
  glEnd();

  glutSwapBuffers();
}

void reshape(int w, int h) {
  glutReshapeWindow(w, w);
  width = w;
  height = h;
  initGL();
}

int main(int argc, char ** argv) 
{
  printf("K = ");
  scanf("%f", &K);
  printf("n = ");
  scanf("%d", &n);

  glutInit(&argc, argv);
  glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE);
  glutInitWindowSize(width, height);
  glutInitWindowPosition(100, 100);
  glutCreateWindow("14020072 Rose");

  initGL();

  glutDisplayFunc(display);
  glutReshapeFunc(reshape);

  glutMainLoop();
}
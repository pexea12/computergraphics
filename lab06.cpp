#include <GL/glut.h>
#include <stdio.h>

void display();
void specialKeys(int key, int x, int y);
void loadTexture(const char * filename);

double rotateX = 0.0;
double rotateY = 0.0;
GLuint texture;

float v[6][4][3] = {
  { 
    { -0.5, -0.5, -0.5 },
    { -0.5, 0.5, -0.5 },
    { 0.5, 0.5, -0.5 },
    { 0.5, -0.5, -0.5 },
  },
  { 
    { 0.5, -0.5, 0.5 },
    { 0.5, 0.5, 0.5 },
    { -0.5, 0.5, 0.5 },
    { -0.5, -0.5, 0.5 },
  },
  { 
    { 0.5, -0.5, -0.5 },
    { 0.5, 0.5, -0.5 },
    { 0.5, 0.5, 0.5 },
    { 0.5, -0.5, 0.5 },
  },
  { 
    { -0.5, -0.5, 0.5 },
    { -0.5, 0.5, 0.5 },
    { -0.5, 0.5, -0.5 },
    { -0.5, -0.5, -0.5 },
  },
  { 
    { 0.5, 0.5, 0.5 },
    { 0.5, 0.5, -0.5 },
    { -0.5, 0.5, -0.5 },
    { -0.5, 0.5, 0.5 },
  },
  { 
    { 0.5, -0.5, -0.5 },
    { 0.5, -0.5, 0.5 },
    { -0.5, -0.5, 0.5 },
    { -0.5, -0.5, -0.5 },
  },
};

float color[6][3] = {
  { 0.0, 1.0, 1.0 },
  { 1.0, 1.0, 1.0 },
  { 1.0, 0.0, 1.0 },
  { 0.0, 1.0, 0.0 },
  { 0.0, 0.0, 1.0 },
  { 1.0, 0.0, 0.0 },
};

float vTex[4][2] = {
  { 0.0, 0.0 },
  { 0.0, 1.0 },
  { 1.0, 1.0 },
  { 1.0, 0.0 },
};

int main(int argc, char * argv[])
{
  glutInit(&argc, argv);
  glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
  glutCreateWindow("14020072 - Texturing");
  glEnable(GL_DEPTH_TEST);

  loadTexture("lena512.bmp");

  glutDisplayFunc(display);
  glutSpecialFunc(specialKeys);

  glutMainLoop();

  return 0;
}

void display() {
  glLoadIdentity();
   
  // Rotate when user changes 
  glRotatef(rotateX, 1.0, 0.0, 0.0);
  glRotatef(rotateY, 0.0, 1.0, 0.0);

  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  for (int i = 0; i < 6; i += 1) {
    glEnable(GL_TEXTURE_2D);
    glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_DECAL);
    glBindTexture(GL_TEXTURE_2D, texture);
    glBegin(GL_POLYGON);
      glColor3f(color[i][0], color[i][1], color[i][2]);
      for (int j = 0; j < 4; j += 1) {
        glTexCoord3f(v[i][j][0], v[i][j][1], v[i][j][2]);
        glVertex3f(v[i][j][0], v[i][j][1], v[i][j][2]);
      }
    glEnd();
    glDisable(GL_TEXTURE_2D);
  }
   
  glFlush();
  glutSwapBuffers();
}

void specialKeys(int key, int x, int y) {
  switch (key) {
    case GLUT_KEY_RIGHT: 
      rotateY += 5; break;
    case GLUT_KEY_LEFT:
      rotateY -= 5; break;
    case GLUT_KEY_UP:
      rotateX += 5; break;
    case GLUT_KEY_DOWN:
      rotateX -= 5; break;
  }

  glutPostRedisplay();
}

void loadTexture(const char * filename) {
  int width, height, size;
  unsigned char header[54];
  unsigned char * data;

  FILE * file;

  file = fopen(filename, "rb");

  if (!file) {
    printf("Cannot open image %s\n", filename);
    return;
  }

  if (fread(header, 1, 54, file) !=54) {
    printf("%s is not a correct BMP file\n", filename);
    return;
  }

  if (header[0] != 'B' || header[1] != 'M') {
    printf("%s is not a correct BMP file\n", filename);
    return;
  }

  size = * (int *) &(header[0x22]);
  width = * (int *) &(header[0x12]);
  height = * (int *) &(header[0x16]);

  size = width * height * 3;

  data = new unsigned char[size];

  fread(data, 1, size, file);

  fclose(file);

  glGenTextures(1, &texture);
  glTexImage2D(
    GL_TEXTURE_2D, 
    0,
    GL_RGB, 
    width, 
    height,
    0, 
    GL_BGR, 
    GL_UNSIGNED_BYTE, 
    data
  );

  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  free(data);
}

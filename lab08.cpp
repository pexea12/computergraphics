#include <stdio.h>
#include <GL/glu.h>
#include <GL/glut.h>

int width = 500;           
int height = 500;

double rotateX = 0.0;
double rotateY = 0.0;

struct Point {
  float x, y, z;
  Point() : x(0.0), y(0.0), z(0.0) {};
  Point(float _x, float _y, float _z) : x(_x), y(_y), z(_z) {}
  void draw() {
    glVertex3f(x, y, z);
  }
  Point oppositeX() {
    return Point(-x, y, z);
  }
};

int total;
int totalTriangles;
Point *p;

int **tri;
float minX = 100.0f, maxX = -100.0f;
float minY = 100.0f, maxY = -100.0f;
float minZ = 100.0f, maxZ = -100.0f;

void readFile(const char * filename) {
  FILE *f;
  f = fopen(filename, "r");
  fscanf(f, "%d", &total);

  p = new Point[total];

  for (int i = 0; i < total; i += 1) {
    float x, y, z;
    fscanf(f, "%f", &x);
    fscanf(f, "%f", &y);
    fscanf(f, "%f", &z);

    if (x < minX) minX = x;
    if (x > maxX) maxX = x;
    if (y < minY) minY = y;
    if (y > maxY) maxY = y;
    if (z < minZ) minZ = z;
    if (z > maxZ) maxZ = z;

    p[i] = Point(x, y, z);
  }

  fscanf(f, "%d", &totalTriangles);
  tri = new int*[totalTriangles];
  for (int i = 0; i < totalTriangles; i += 1) {
    tri[i] = new int[3];
    fscanf(f, "%d", &tri[i][0]);
    fscanf(f, "%d", &tri[i][1]);
    fscanf(f, "%d", &tri[i][2]);
  }

  fclose(f);
}

void drawTriangle(int *t) {
  glBegin(GL_LINE_LOOP);
    p[t[0]].draw();
    p[t[1]].draw();
    p[t[2]].draw();
  glEnd();
}

void drawOppositeXTriangle(int *t) {
  glBegin(GL_LINE_LOOP);
    p[t[0]].oppositeX().draw();
    p[t[1]].oppositeX().draw();
    p[t[2]].oppositeX().draw();
  glEnd();
}

void initGL()
{
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  glOrtho(minX - 3.0, maxX + 3.0, minY - 3.0, maxY + 3.0, minZ - 3.0, maxZ + 3.0);
  glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
  glShadeModel(GL_SMOOTH);
  glViewport(0, 0, width, height);
}

void reshape(int w, int h)
{
  glutReshapeWindow(w, w);
  width = w;
  height = w;
  initGL();   
}

void display() {
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  glMatrixMode(GL_PROJECTION);
  glColor3f(1.0, 1.0, 1.0);

  glRotatef(rotateX, 1.0, 0.0, 0.0);
  glRotatef(rotateY, 0.0, 1.0, 0.0);

  for (int i = 0; i < totalTriangles; i += 1) {
    drawTriangle(tri[i]);
    drawOppositeXTriangle(tri[i]);
  }

  glutSwapBuffers();
  glutPostRedisplay();
}


void specialKeys(int key, int x, int y) {
  switch (key) {
    case GLUT_KEY_RIGHT: 
      rotateY += 5; break;
    case GLUT_KEY_LEFT:
      rotateY -= 5; break;
    case GLUT_KEY_UP:
      rotateX += 5; break;
    case GLUT_KEY_DOWN:
      rotateX -= 5; break;
  }

  glutPostRedisplay();
}


int main(int argc, char **argv)
{
  glutInit(&argc, argv);   
  glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE);
  glutInitWindowSize(width, height); 
  glutInitWindowPosition(100, 100);
  glutCreateWindow("14020072 Face");

  initGL();   
  readFile("face.dat");

  glutDisplayFunc(display);              
  glutReshapeFunc(reshape);    
  glutSpecialFunc(specialKeys);        						
      
  glutMainLoop();				
}

